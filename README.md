# Assignment Module 4
The assignment uses the database created in module 3 which has two tables: `Users` and `Projects`.

The class uses the four CRUD operations on each table; first it Creates, Reads and Updates the users and then it Creates, Reads and Updates a project using the id of the newly created user, then it deletes the user and the project gets deleted as well thanks to cascading behaviors.   