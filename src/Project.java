import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Project {
	private int id;
	private String name;
	private int ownerID;
	private String domain;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	private Connection conn = null;
	private Statement stmt = null;
	
	Project () {
		try {
			this.conn = DriverManager.getConnection("jdbc:mysql://51.254.211.97/module3?user=andre&password=pazzword");
			
			this.stmt = this.conn.createStatement();
			
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public ArrayList<Project> getProjects() {
		try {
			String sql = "Select * from projects";
			
			ResultSet rs = this.stmt.executeQuery(sql);
			
			ArrayList<Project> users = new ArrayList<Project>();
			
			while (rs.next()) {
				Project item = new Project();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setOwnerID(rs.getInt("ownerID"));
				item.setDomain(rs.getString("domain"));
				
				users.add(item);
			}
			
			return users;
		} catch (Exception ex) {
			System.out.print("error on getProject" + ex.getMessage());
			return null;
		}
	}
	
	public int createProject(String newName, int ownerID, String domain) {
		int newid = 0;
		
		try {
			String sql = "Insert into projects (name, ownerID, domain) values ('" + newName + "', '" + ownerID + "', '" + domain + "')";
			
			int rowaffected = this.stmt.executeUpdate(sql);
			
			if (rowaffected > 0) {
				sql = "Select max(id) as id from projects";
				
				ResultSet rs = this.stmt.executeQuery(sql);
				
				if (rs.next()) {
					newid = rs.getInt("id");
				} else {
					throw new Exception("Error to insert Data");
				}
			}
		} catch (Exception ex) {
			System.out.println("Error on createProject: " + ex.getMessage());
		}
		return newid;
	}
	
	public boolean deleteProject (int id) {
		try {
			String sql = "Delete from projects where id=" + id;
			
			int rowaffected = this.stmt.executeUpdate(sql);
			
			if (rowaffected > 0) {
				return true;
			} else {
				throw new Exception ("Error deleting data");
			}
		} catch (Exception ex) {
			System.out.println("Error on deleteProject: " + ex.getMessage());
			return false;
		}
	}
	
	public void updateProject (int newProject, String name, String domain) {
		try {
			String sql = "Update projects set name= '" + name + "', domain = '" + domain + "' where id=" + newProject;
					
			int rowaffected = this.stmt.executeUpdate(sql);
			
			if (rowaffected > 0) {
				System.out.println("Element has been updated");
			} else {
				throw new Exception ("Error updating data");
			}
		} catch (Exception ex) {
			System.out.println("Error on updateProject: " + ex.getMessage());
		}
		
	}
}
