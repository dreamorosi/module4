import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class User {
	private int id;
	private String fullName;
	private String email;
	private int verified;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getVerified() {
		return verified;
	}

	public void setVerified(int verified) {
		this.verified = verified;
	}

	private Connection conn = null;
	private Statement stmt = null;
	
	User () {
		try {
			this.conn = DriverManager.getConnection("jdbc:mysql://51.254.211.97/module3?user=andre&password=pazzword");
			
			this.stmt = this.conn.createStatement();
			
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public ArrayList<User> getUsers() {
		try {
			String sql = "Select * from users";
			
			ResultSet rs = this.stmt.executeQuery(sql);
			
			ArrayList<User> users = new ArrayList<User>();
			
			while (rs.next()) {
				User item = new User();
				item.setId(rs.getInt("id"));
				item.setFullName(rs.getString("fullName"));
				item.setEmail(rs.getString("email"));
				item.setVerified(rs.getInt("verified"));
				
				users.add(item);
			}
			
			return users;
		} catch (Exception ex) {
			System.out.print("error on getUser" + ex.getMessage());
			return null;
		}
	}
	
	public int createUser(String newFullName, String newEmail) {
		int newid = 0;
		
		try {
			String sql = "Insert into users (fullName, email) values ('" + newFullName+ "', '" + newEmail+ "')";
			
			int rowaffected = this.stmt.executeUpdate(sql);
			
			if (rowaffected > 0) {
				sql = "Select max(id) as id from users";
				
				ResultSet rs = this.stmt.executeQuery(sql);
				
				if (rs.next()) {
					newid = rs.getInt("id");
				} else {
					throw new Exception("Error to insert Data");
				}
			}
		} catch (Exception ex) {
			System.out.println("Error on createUser: " + ex.getMessage());
		}
		return newid;
	}
	
	public boolean deleteUser (int id) {
		try {
			String sql = "Delete from users where id=" + id;
			
			int rowaffected = this.stmt.executeUpdate(sql);
			
			if (rowaffected > 0) {
				return true;
			} else {
				throw new Exception ("Error deleting data");
			}
		} catch (Exception ex) {
			System.out.println("Error on deleteUser: " + ex.getMessage());
			return false;
		}
	}
	
	public void updateUser(int newUser, String fullName, String email) {
		try {
			String sql = "Update users set fullName= '" + fullName + "', email = '" + email + "' where id=" + newUser;
					
			int rowaffected = this.stmt.executeUpdate(sql);
			
			if (rowaffected > 0) {
				System.out.println("Element has been updated");
			} else {
				throw new Exception ("Error updating data");
			}
		} catch (Exception ex) {
			System.out.println("Error on updateUser: " + ex.getMessage());
		}
		
	}
}
