import java.util.ArrayList;

public class main {

	public static void main(String[] args) {
		User x = new User();
		Project y = new Project();
		
		// CREATE USER
		int newUser = x.createUser("Julie Doe", "jdoe2@gmail.com");

		// READ USERS
		ArrayList<User> items = x.getUsers();
		
		for (User item: items) {
			System.out.println(item.getFullName());
		}
		
		// UPDATE USER
		x.updateUser(newUser, "Jenny Doe", "jdoe3@gmail.com");
		
		// CREATE PROJECT
		int newProject = y.createProject("Project 12", newUser, "");
		
		// READ PROJECTS
		ArrayList<Project> projects = y.getProjects();
		
		for (Project proj: projects) {
			System.out.println(proj.getName());
		}
		
		// UPDATE USER
		y.updateProject(newProject, "Project 13", "http://ciao.com");
		
		// DELETE USER
		if (x.deleteUser(newUser)) {
			System.out.println("Element has been deleted");
		}
		
		// DELETE PROJECT
		// Not using because the project get deleted by cascade
		/*
		if (y.deleteProject(newProject)) {
			System.out.println("Element has been deleted");
		}
		*/
	}

}
